# Technical Documentation

## Introduction

![Outline of software components](imgs/diagram.png)

This document outlines the process of creating the dataset that ultimately indicates if Australian online job vacancies are (implicitly) using discriminatory language, from collecting online job adverts to the processing & analysis of their content, such as if certain discriminatory phrases have been used.

As an outline, the process pipeline generally involves:

1. Searching and collecting job adverts from well-known Australian job search websites.
2. Detecting job vacancies which are duplicates/copies of other ads and "grouping" them together, as a job group.
3. Process text of ads within a job group, parsing and analyse if certain content is present in that job group. For example: expected education level of job and the degree of a soft skill being implied for a job position.
4. Call external API services and websites to find the number of employees and ANZSCO code of company names provided by job adverts.

The documentation is structured in the order of the pipeline below, from collecting a job ad presented online to extracting and adding the "job group" into the final dataset for further analysis by Stata.

![General outline of pipeline](imgs/pipeline-outline.png)

## Web scrapers
To be able to produce the dataset, online job vacancies must be collected from job search websites (this is called 'scraping'). Unfortunately, these sites usually do not provide an API to directly collect posted job vacancies, likely due to prevent excessive data scraping from the sites. Thus, scripts have been written to search and collect information of job ads.

These attributes include:

* Job vacancy title
* Description
* Salary rate and type (e.g. annually, hourly)
* Location of employment
* Company who had posted the vacancy
* Job category
* Employment type (full time, part time etc)

After collecting these attributes for a given job vacancy, these are stored into the database (here we use Postgres). 

To ensure the ads are actually seen by job seekers and hence representative of Australian online job ads, we collect ads from three well-known Australian job search sites that are also relatively easy to scrape from:

1. [Seek](https://www.seek.com.au/)
2. [CareerOne](https://www.careerone.com.au/)
3. [Jora](https://au.jora.com/) 

Some attributes, such as the base salary suggested and the company of the job ad, are not directly given by Jora and CareerOne. Sometimes this information is within the job description, but is not directly stated as an attributed from the website. Consequently, these need to be extracted via NLP libraries later in the pipeline process.

Additionally, multiple job ads may be reposted to multiple job sites. This may introduce biases into our dataset, and potentially over-represent certain trends when analysed by Stata. To address this, we group very similar job ads into a **job group**.

## Job grouper tool
To improve reachability of online job postings to potential job seekers, the same ad may be reposted under various job search sites, or under the different categories in the same website. Sometimes these ads are slightly rephrased, which complicates detection of duplicate job ads. To detect such duplicates, a program was written to search and group very similar ads currently store in the database.

The job grouper tool periodically checks if any new ads have been added into the database. If there are job ads which have not been added into a job group, then a random job ad is chosen.

Afterwards, other potentially similar ads stored in the database are searched for, which:
1. Have the same company name, and
2. Has a 'similar title' (a similarity metric using trigrams is used to calculate this)

We consider two ads to have similar titles if the "trigram similarity" between the title of the two ads is greater than $50\%$.

> **Example:** Trigram similarity 
>
> To calculate the similarity score between two titles, trigrams of both titles are calculated (in the form of a set). Additionally, two spaces are prefixed and one space suffixed before calculating their trigrams. This calculation is provided by [PostgreSQL's pg_trgm extension](https://www.postgresql.org/docs/13/pgtrgm.html).
>
> Suppose we had two job ads A and B with respective titles "Legal Secretary" and "Legal Executive".
>
> Job A would have the trigrams $\{\text{l, le, leg, ega, gal, al, s, se, sec, ecr, cre, ret, eta, tar, ary, ry}\}$. It has a cardinality of $16$.
>
> Job B would have trigrams $\{\text{l, le, leg, ega, gal, al, e, ex, exe, xec, ecu, cut, uti, tiv, ive, ve}\}$. It also has a cardinality of $16$.
>
> Both trigrams share $\{\text{l, le, leg, ega, gal, al}\}$ in common (coming from the word 'legal'). Thus:
> $$
> \text{Similarity score} = \frac{|A \cap B|}{|A \cup B|} = \frac{6}{16 + 16 - 6} \approx 0.23
> $$
>

Occassionally, the chosen job ad may not have explictly specified a company (only occuring from Jora job ads). In this situation, the Jobs API is called to find the company the chosen ad is for, by looking at its description content. We use this company name, if the Jobs API manages to find a name within the description text.

### Detecting duplicates
After searching for potentially similar ads, the job vacancy's description is compared against the other ads, determined by a description similarity score. The other job ad is considered a **duplicate** of the job ad if the description score is greater than $90\%$.

The Levenshtein distance is used to calculate string similarity of job descriptions, which calculates the smallest number of edits (character insertion, deletion or a substitution) needed to make one string match the other. Since job descriptions come with varying text lengths, the Levenshtein distance of both descriptions is normalised using the following formula:

$$
\text{Description similarity} = 1 - \frac{\text{lev}(A, B)}{\max(\text{length}(A,B))} \in [0,1]
$$

where:

* $A$ and $B$ are the description of any two job ads
* $\text{lev}(\cdot, \cdot)$ is the Levenshtein distance function
* $\text{length}(\cdot, \cdot)$ is the length of any two strings/texts.
* If two texts are identical then no edits are needed and thus $\text{lev}(A, B) = 0$, leading to a similarity score of $1$ (and vice-versa).

### Creating the job group
After searching and checking for any duplicate job ads, a job group is created, which groups the job ad and its duplicates. If no duplicate job ads were found, then only one ad is included in that newly created job group. This information is stored in the database.

Later down in the pipeline, some very similar (or exactly equivalent) job ads provide information which are not found by other ads within a job group. For example, this might be the employment type given by Seek, which may not have been stated by ads from Jora/CareerOne.

## Processing job groups
Perhaps the most involved part of the pipeline process, which aims to parse and extract useful information from the unstructuredtext of job ads in a job group. Relevant fields to extract are:

* [ANZSCO (Australian and NZ Standard Classification of Occupations)](https://www.abs.gov.au/AUSSTATS/abs@.nsf/allprimarymainfeatures/FCC055588D3EBA19CA2584A8000E7889) of job group
* Employment type of job (full time, part time, casual etc.)
* Minimum number of years of experience expected of any skill for job
* Expected minimum and maximum age to apply for job (if specified)
* Education level of job (e.g. high school, diploma, university)
* Degree of soft skills suggested by job ads
* Number of discriminatory phrases/words present in a job ad
* Base salary

The job group is partially analysed by the [spaCy](https://spacy.io/), an open-source Python library which provides prepared functionality to parse unstructured text, including tokenisation, part-of-speech tagging and dependency parsing (analysing dependencies/relations between words in sentences). The library will be primilarily used to extract certain relevant fields, such as base salary if not directly provided by the job search site, and the suggested minimum years of experience of a skill.

Moreover, a pre-trained transformer model derived from BERT is used, namely the [Python library SentenceTransformers](https://www.sbert.net/). The model is currently mainly used to extract the similarilty of two phrases/sentences of a certain skill. Its use to determine if a sentence infers a certain soft skill will be discussed in more detail in this section.

Certain fields, such as classifying the ANZSCO classification and education level, are done using simple regular expression matching, without using of grammatical features of the text. This was decided on the basis that it would be sufficient enough to accurately determine these fields without needing to use more resource-intensive methods, such as using a tuned transformer model perform a downstream task on our relatively large dataset.

This section goes through explaning the of process of extracting fields.

### Employment type
A majority of job ads from explicitly provide the employment type, making it easy to parse the employment type.

We classify a job ad into these employment types:

* Full time
* Part time
* Fixed term (e.g. contracts)
* Casual
* Other (e.g. internships, volunteering ads)

Different job search sites have different labels for the employment type of an ad, so it is a matter of mapping it to a standardised value. This mapping is specified by the [`work_types.json`](https://gitlab.com/aphan1um/core-data/-/blob/master/jobs-api/work_types.json) JSON file, where the mapping depends on where the job ad originated from.

### Expected education level
We classify the minimum/expected education level of a job group into three groups:

* High school
* Certificate (e.g. diploma)
* University

The description of a job ad matched against a list of [spaCy pattern rule base matching patterns](https://spacy.io/usage/rule-based-matching), which allow to utilise certain properties of the text parsed from spaCy's default NLP pipeline. This includes being able to use the lemmatised form of the description's tokens/words.

The list of spaCy rules is specified by the [`spacy-education-matcher.json`](https://gitlab.com/aphan1um/core-data/-/blob/master/jobs-api/spacy-education-matcher.json) JSON file.


> **Example: JSON file explained**
>
> Based on [documentation regarding spaCy's rule based matching](https://spacy.io/usage/rule-based-matching), the specification `spacy-education-matcher.json` has this section:
> 
> ``` json
> "certificate": [
>   [
>     {
>       "LOWER": "advanced",
>       "OP": "!"
>     },
>     {
>       "LOWER": "diploma"
>     }
>   ],
>   [
>     {
>       "LOWER": {
>         "REGEX": "^cert"
>       }
>     },
>     {
>       "LOWER": {
>         "REGEX": "^((i|1){1,3}|iv|[1234])$"
>       }
>     }
>   ]
> ]
> ```
>
> This specifies two rules for a job to be classified with a certification education level:
>
> 1. The job's description must contain the word "diploma" but not prefixed with "diploma", as an advanced diploma may actually be at level of a university degree.
> 2. The description contains a word starting with "Cert", followed by a number 1-4 (or in Roman numerals).
>

In the case no spaCy rule matches, the inferred ANZSCO code may be used to determine the minimum education level required. For example, the ANZSCO classification of a 'university lecturer' would clearly need expect a university degree from a candidate.


### ANZSCO
ANZSCO is a system of job occupations developed jointly by Australian Bureau of Statistics and Statistics New Zealand, aimed to allow classification and comparison of industry statistics internationally. Multiple versions exist for this standard. Here we use version 1.3 to classify job groups.

With a large number of classifications it would be infeasible to train a model reliably infer the occupation of ads. Thus, a simpler method relying on regular expressions is used to detect the occupation of a job ad.

By using provided properties of the contents of a job ad (e.g. title, description, category/sub-category if available), its content is matched against a [list of rules provided by a JSON file](https://gitlab.com/aphan1um/core-data/-/blob/master/jobs-api/coding_anzsco.json). Some rules contain sub-rules which provide a more specific and/or accurate ANZSCO classification if the rule matches for a job ad. As the rules are in the form a list, the rules are ran starting from the first rule in the `coding_anzsco.json` list.


> **Example 1: Retrieving the ANZSCO code**
>
> Suppose the title of a job ad in some job group is the title "Compliance Financial Manager". Additionally, the job ad has the word "financial" in its job description. When the list of rules are matched against the ad, we encounter this rule:
>
> ``` json
> {
>   "title": "\\bcompliance",
>   "description": [
>     "bank",
>     "financial"
>   ],
>   "_value": 221200
> }
> ```
>
> Which specifies the title should contain the word "compliance" and the ad's description should contain either "bank" or "financial". 
>
> Since the job ad matches these two conditions, the ad is classified with ANZSCO code 221200 (given by the `_value` field).
>


> **Example 2: Child rules**
>
> Suppose a job ad has the title "ICT Senior Project Manager" with a category "Information and Technology (IT)" and we encounter this rule in the JSON list:
>
> ``` json
> {
>  "title": "project.*manager",
>  "_child": [
>    {
>      "title": "\\bIC?T\\b",
>      "_value": 135112
>    },
>    {
>      "category": "\\bmining\\b",
>      "_value": 133513
>    }
>  ]
> }
> ```
>
> The parent rule expects the title to contain "project", followed by "manager". This is true for our job ad, but this rule does not have a specified `_value`, but two children of this rule. These two child rules expects the word "IT"/"ICT" in the job ad's title, or the string "mining" to be mentioned in the job ad's category.
>
> In this case only the first child rule is true, and thus this job ad is classified with the ANZSCO code 135112.
>


If there are no rules which succeed, the job group is given the code 99888, which represents that it is 'inadequately described'. Either the list needs to be expanded to be able classify the ad, or the job ad may itself be too broad to be easily classified using a rule system based on regular expressions. <u>It is hoped over time this issue will be  less of an issue as new rules are added to the ANZSCO JSON list.</u>


### Expected years of experience
The amount of experience can be worded in numerous ways:

* "Have at least 1 year of experience as job X"
* "5+ years of commercial refrigeration experience"
* "You're a nurse with minimum of 3 years full time nursing experience"
* "[...] 15+ years in technology generally"

And then there are false positives:
* "We are a family company of 25 years:
* "We are company with more than twenty years of experience"

An obvious approach would be to find numbers with the word "experience" nearby, but based on the false positives this is not always toward the candidate. Moreover, there are numerous ways to phrase the expected experience. Consequently, we use [spaCy's dependency parser](https://spacy.io/usage/rule-based-matching#dependencymatcher) to be able to determine if the number specified in a job's description is actually referring to the number of years of experience needed from a candidate or otherwise.

More generally, spaCy's dependency matcher allows us to create rules for the dependency tree created from the tokenised text of a description of a job ad.


![Visualisation of a dependency tree of a sentence](./imgs/spacy-dependency-example.png)

The specification of  [`spacy-experience-dep.json`](https://gitlab.com/aphan1um/core-data/-/blob/master/jobs-api/spacy-experience-dep.json), and was developed using trial-and-error.

> **Example:**
>
> Suppose we had the sentence "Minimum 10 years' experience managing electrical construction activities on project sites."
>
> This sentence produces this dependenc parse:
> 
> ![](imgs/spacy-dependency-example-2.png)
>
> Which matches one of the rules specified within `spacy-experience-dep.json` file: 
>
> ``` json
> {
>   "RIGHT_ID": "experience_noun",
>   "RIGHT_ATTRS": {
>     "LEMMA": "experience"
>   }
> },
> {
>   "LEFT_ID": "experience_noun",
>   "REL_OP": ">",
>   "RIGHT_ID": "anchor_time",
>   "RIGHT_ATTRS": {
>     "LEMMA": {
>       "IN": [
>         "month",
>         "year"
>       ]
>     }
>   }
> },
> {
>   "LEFT_ID": "anchor_time",
>   "REL_OP": ">>",
>   "RIGHT_ID": "number",
>   "RIGHT_ATTRS": {
>     "DEP": {
>       "IN": [
>         "quantmod",
>         "nummod"
>       ]
>     },
>     "LIKE_NUM": true
>   }
> },
> {
>   "LEFT_ID": "experience_noun",
>   "REL_OP": ";*",
>   "RIGHT_ID": "final_word",
>   "RIGHT_ATTRS": {
>     "POS": {
>       "IN": [
>         "NOUN",
>         "ADJ",
>         "PROPN"
>       ]
>     },
>     "LEMMA": {
>       "NOT_IN": [
>         "month",
>         "year"
>       ]
>     },
>     "DEP": {
>       "NOT_IN": [
>         "nsubj"
>       ]
>     }
>   }
> }
> ```
>
> In other words, this rule expects:
>
> * The word "experience" in lemmatised form.
> * The word "month" or "year" as the child of the word "experience"
> * A numeric value with dependency tag "quantmod" or "nummod", where the number is the child of "year/month"
> * There is a noun, adjective or pronoun that is a descendent of "experience" in the dependency tree, for which its child is:
>     * Not the word "month" or "year", and
>     * The dependency tag is not "nsubj" (noun subject)
>     * In this case the descendent which satisfies the 3 conditions above is the word "minimum".
>

### Number of discriminatory phrases
When we refer to discriminatory phrases, we refer to these types:

* Ableist language
* Genered language (this is not necessarily explicitly sexist language, but rather wording which may dissuade certain genders from applying for certain job ads)
* **[TODO]** Language which may discourage candidates with "precarious" employment histories.

To detect the presence of potentially implicit discriminatory phrases we use spaCy's rule based matching. These rules are specified by the [`spacy-discrimination-matcher.json`](https://gitlab.com/aphan1um/core-data/-/blob/master/jobs-api/spacy-discrimination-matcher.json) file.

Currently there are three groups currently in use to indicate different categories of discrimination:

| Category | Description |
| -------- | ----------- |
| disability-general | Contains rules that contain terms which are considered derogatory towards disabled communnities, as suggested by Australian disability organisations.       |
| gender-implicit-masculine   | Contains words which were found to discourage female applicants from applying to job advertisements (from the paper "*Evidence That Gendered Wording in Job Advertisements Exists and Sustains Gender Inequality*")  |
| gender-implicit-feminine | Contains words which were found to affect job ad appeal from both genders (from the paper "*Evidence That Gendered Wording in Job Advertisements Exists and Sustains Gender Inequality*") |

The rules per category are ran on the tokenised text of the description of a job ad. A count of the total number of times a rule per category was true on a group of tokens is then formed.


### Degree of soft skills present
This field aims to detect whether certain soft skills (e.g. able to work independent or as a team) are suggested or expected for a job vacancy, based on its content. This field will be ultimately used to verify one of the hypotheses related soft skills and level of (implicit) discrimination in the language of job ads.

Currently this is the only field which uses a pretrained BERT-based transformer to get sentence embeddings and calculate sentence similarity between a pair of phrases. The list of soft skills and their phrases is defined by [`soft-skills-compiled.json`](https://gitlab.com/aphan1um/core-data/-/blob/master/jobs-api/soft-skills-compiled.json) file. These phrases were provided by the paper "*Responsible team players wanted: An analysis of soft skill requirements in job advertisements*".

> **Example:**
>
> Within the `soft-skills-compiled.json` file, there is a section:
> ``` json
> "flexible": [
>   "versatility",
>   "flexible",
>   "ability to be flexible",
>   "flexibility"
> ]
> ```
> Which represents the soft skill "flexible" and it contains four phrases, which suggest the skill of being flexible at a job.
>

To determine the degree (a number between 0-1) of a soft skill of a job ad:

1. The description of a job ad is tokenised and 4-grams are formed.
2. Using the transformer, each 4-gram are converted into vector embeddings.
3. For each vector of a 4-gram:
     * A similarity score is calculated against each phrase of a soft skill, in its vector form.
     * This is done for each soft skill and its phrases.
4. Collate the similarity scores calculated for each soft skill into a list.

Afterwards, a 95th percentile (ensure the soft skill is sufficiently evident in the job ad's description) is calculated per soft skill, which defines the degree of a certain soft skill present for a job ad.


## Gathering company information
Certain fields, including:

* [ANZSIC (Australian and New Zealand Standard Industrial Classification)](https://www.abs.gov.au/AUSSTATS/abs@.nsf/allprimarymainfeatures/D249EC2A7DC203BACA257B9500133E91) of a company, and
* The (approximate) number of employees of a company
* Firm ownership type of company (e.g. public, private, non-profit, government)

Cannot be extracted from the content of a job advertisement alone, and is available from other external websites. In this situation, a [script was written](https://gitlab.com/aphan1um/geo-api) to be able gather the above fields based on the given company name.

Firm ownership types is publicly available on the [Australian Business Register (ABR) website](https://abr.business.gov.au/) via the business' ABN. Unfortunately the [ANZSIC is non-public data](https://www.abr.gov.au/business-super-funds-charities/applying-abn/your-abn-details-abr) and thus other non-government websites have been searched to infer the ANZSIC code for a company.

The outline to gather these three fields is as follows:

1\. Given a company name "Company X", Google the phrase "Company X" and retrieve any ABNs (Australian Business Numbers) from the first page of the Google result.

2\. For each ABN listed in the Google result:

   * Retrieve the firm type of the company via the Australian Business Register.
   * Retrieve the state and postcode of the company's ABN.

3a. Using the company name and state & postcode collected from ABR:

   * Search for Australian companies listed from [Dun & Bradstreet](https://abr.business.gov.au/).
   * Choose the first company listed in the search result.
   * If present, retrieve the amount of employees and the industry categories listed for that company.

3b. If the company is not listed on Dun & Bradstreet, search for the Google query "Company X (suburb name of postcode)".
  * If an "organised search result page" (OSRP) is present, retrieve the industry type of a job if it exists.

4\. For all industry categories collected from Dun & Bradstreet or Google, map the industry category to an ANZSIC code based on [`anzsic-mapping.json`](https://gitlab.com/aphan1um/core-data/-/blob/master/geo-api/anzsic-mapping.json) JSON file. The ANZSIC classification that appears the most is chosen as the suggested ANZSIC code for the company.
