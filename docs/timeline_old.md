# Timeline (old)

## Semester Break (2021)

| Date | Intended goal |
| ---- | ------------- |
| 15th November 2021 | Draft technical documentation and timeline written |
| 24th - 25th November | Draft literature review presented |
| 27th December | Literature review written to good quality |

## Semester Break (2022)
| Date | Intended goal |
| ---- | ------------- |
| 7th January 2022 | Completion of Stage 1A |
| 24th January | Methodology section draft written |
| 31st January | Completion of Stage 1B |
| Sometime in February | Completion of Stage 2 |

## TODO Pile
This section details current tasks which have reached my attention and need to be resolved.

### Stage 1A
Refine system, prepare analysis of dataset.

#### Thesis
- [ ] Complete literature review to adequate quality
- [ ] Gather studies to assist improvement of pipeline process

#### Pipeline
##### Features
- [x] Add ANZSCO row to dataset
- [x] Add employee count row to dataset
- [ ] Improve Geo API
    * [ ] Use location info of job advertisement to assist in finding company
    * [ ] Deal with recruitment agencies
- [ ] Improve Jobs API
  * [ ] Add more phrases to detecting disability
  * [ ] Further incorporate transformer into detection of 
  * [ ] Find a way to detect signs a job ad may discourage 'precarious' candiates
- [ ] Add entries and sufficiently complete the ANZSIC and ANZSCO JSON list

##### Bugs
- [ ] Fix Unix date shown incorrect date for final CSV dataset
- [ ] Fix job ads which do not specify an explicit location
- [ ] Fix issue with hanging geo-processor-company script

#### Other
- [ ] Determine if jobs from 2020 should be added to dataset
- [ ] Learn Stata and process to analyse dataset

### Stage 1B
This stage aims to finalise the entire pipeline process and get it to process the dataset.

- [ ] Methodology section draft written
- [ ] Improve performance of pipeline to run on final dataset
- [ ] Fix any outstanding issues and bugs of pipeline found during Stage 1A
- [ ] Learn Stata and process to analyse dataset

### Stage 2
This aims to analyse the dataset, and begin to write up the results and analysis part of the thesis.

- [ ] Result and analysis section draft written
