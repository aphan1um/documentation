# Timeline (current)

## Semester 1 (2022)

| Date | Goal |
| ---- | ---- |
| 15th April | Subset of complete dataset ready for analysis |
| 15th May | All Stage 1A tasks completed |
| 23th May | Complete draft literature review ready |

The tasks to complete are grouped into stages, and have a deadline when all of its tasks need to be completed.

## Stage 1A
Refine system, fix bugs and prepare analysis of dataset.

- [ ] Improve Geo API
    * [ ] Use location info of job advertisement to assist in finding company
    * [ ] Deal with recruitment agencies
- [ ] Fix current bugs
    * [x] Fix Unix date shown incorrect date for final CSV dataset
    * [ ] Fix job ads which do not specify an explicit location
    * [ ] Fix issue with hanging geo-processor-company script
- [ ] Find a way to detect signs a job ad may discourage 'precarious' candiates

#### Stage 1B
Improve features before reaching stage 2, which will focus on analysis dataset.

- [ ] Add jobs from 2020 into separate processed dataset
- [ ] Add more phrases to detecting disability


## Daily work-list
Tasks to done on a daily basis, to gradually improve feature detection and reliability.

1. Reduce unrecognised **ANZSIC** results; increase accuracy of ANZSIC classification
2. Reduce unrecognised **ANZSCO** results; increase accuracy of ANZSCO classification
3. Gather studies to assist improvement of pipeline process


<!-- #### Pipeline
##### Features

- [ ] Improve Jobs API
  * [ ] Further incorporate transformer into detection of 


### Stage 1B
This stage aims to finalise the entire pipeline process and get it to process the dataset.

- [ ] Methodology section draft written
- [ ] Improve performance of pipeline to run on final dataset -->


